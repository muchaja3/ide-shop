package cvut.fel.ideshop.service;


import cvut.fel.ideshop.model.database.entity.Change;
import cvut.fel.ideshop.utils.Generator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertSame;

@SpringBootTest
@Transactional
public class ChangeServiceImpTest {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private ChangeService service;

    @BeforeEach
    public void setUp() {

    }

    @Test
    @DisplayName("Testing created change exists")
    public void createAccount_TestImplementation() {
        Change change = Generator.getInstance().createChange();
        em.persist(change);
        Change expectedById = service.findById(change.getId());
        assertSame(expectedById, change);
    }
}
