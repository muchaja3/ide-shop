package cvut.fel.ideshop.service;

import cvut.fel.ideshop.model.database.entity.Basket;
import cvut.fel.ideshop.utils.Generator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertSame;

@SpringBootTest
@Transactional
public class BasketServiceImpTest {
    @PersistenceContext
    private EntityManager em;

    @Autowired
    private BasketService service;

    @BeforeEach
    public void setUp() {

    }

    @Test
    @DisplayName("Testing created basket exists")
    public void createAccount_TestImplementation() {
        Basket basket = Generator.getInstance().createBasket();
        em.persist(basket);
        Basket expectedById = service.findById(basket.getId());
        assertSame(expectedById, basket);
    }
}
