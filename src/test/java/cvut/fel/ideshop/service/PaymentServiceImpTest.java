package cvut.fel.ideshop.service;

import cvut.fel.ideshop.model.database.entity.Payment;
import cvut.fel.ideshop.utils.Generator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertSame;

@SpringBootTest
@Transactional
public class PaymentServiceImpTest {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private PaymentService service;

    @BeforeEach
    public void setUp() {

    }

    @Test
    @DisplayName("Testing created payment exists")
    public void createAccount_TestImplementation() {
        Payment payment = Generator.getInstance().createPayment();
        em.persist(payment);
        Payment expectedById = service.findById(payment.getId());
        assertSame(expectedById, payment);
    }
}
