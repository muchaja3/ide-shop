package cvut.fel.ideshop.service;

import cvut.fel.ideshop.model.database.entity.Account;
import cvut.fel.ideshop.utils.Generator;
import cvut.fel.ideshop.utils.Generator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertSame;


@SpringBootTest
@Transactional
public class AccountServiceImpTest {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private AccountService service;

    @BeforeEach
    public void setUp() {

    }

    @Test
    @DisplayName("Testing created account exists")
    public void createAccount_TestImplementation() {
        Account account = Generator.getInstance().createAccount();
        em.persist(account);
        Account expectedById = service.findById(account.getId());
        assertSame(expectedById, account);
    }
}
