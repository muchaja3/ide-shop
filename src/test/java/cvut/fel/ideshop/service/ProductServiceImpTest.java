package cvut.fel.ideshop.service;

import cvut.fel.ideshop.model.database.entity.Product;
import cvut.fel.ideshop.utils.Generator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertSame;

@SpringBootTest
@Transactional
public class ProductServiceImpTest {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private ProductService service;

    @BeforeEach
    public void setUp() {

    }

    @Test
    @DisplayName("Testing created product exists")
    public void createAccount_TestImplementation() {
        Product product = Generator.getInstance().createProduct();
        em.persist(product);
        Product expectedById = service.findById(product.getId());
        assertSame(expectedById, product);
    }

}
