package cvut.fel.ideshop.utils;

import cvut.fel.ideshop.model.database.entity.*;

import java.util.Random;

public class Generator {

    private static Generator INSTANCE;

    private final Random RANDOM = new Random();

    public static Generator getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Generator();
        }
        return INSTANCE;
    }

    public Long randomLong() {
        return RANDOM.nextLong();
    }

    public Account createAccount() {
        final Account account = new Account();
        account.setId(6);
        account.setFirstname("Dmitrij");
        account.setLastname("Rastvorov");
        account.setEmail("dimar@mail.com");
        account.setPassword("********");
        return account;
    }

    public Basket createBasket() {
        final Basket basket = new Basket();
        basket.setId(6);
        basket.setProductsAmount(10);
        basket.setTotalPrice("1000$");
        return basket;
    }

    public Change createChange() {
        final Change change = new Change();
        change.setId(6);
        change.setOldDescription("Well, I don't know why...");
        change.setOldName("CLion");
        change.setOldPrice("562$");
        return change;
    }

    public Payment createPayment() {
        final Payment payment = new Payment();
        payment.setId(6);
        payment.setInfo("This is your product, sir");
        payment.setType("IDE");
        return payment;
    }

    public Product createProduct() {
        final Product product = new Product();
        product.setId(6);
        product.setName("C/C++");
        product.setPrice("666$");
        product.setDescription("Well, here we go again");
        return product;
    }
}
