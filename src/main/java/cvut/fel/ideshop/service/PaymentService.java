package cvut.fel.ideshop.service;

import cvut.fel.ideshop.model.database.entity.Payment;

public interface PaymentService {
    Payment findById(Integer id);

    Iterable<Payment> findAll();

    void make(Payment payment);

    void change(Integer id, Payment payment);

    void delete(Integer id);
}
