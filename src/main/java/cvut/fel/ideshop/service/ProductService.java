package cvut.fel.ideshop.service;

import cvut.fel.ideshop.model.database.entity.Product;

public interface ProductService {
    Product findById(Integer id);

    Iterable<Product> findAll();

    void make(Product product);

    void change(Integer id, Product product);

    void delete(Integer id);
}
