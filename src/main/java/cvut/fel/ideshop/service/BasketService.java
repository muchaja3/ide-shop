package cvut.fel.ideshop.service;

import cvut.fel.ideshop.model.database.entity.Basket;

public interface BasketService {
    Basket findById(Integer id);

    Iterable<Basket> findAll();

    void make(Basket basket);

    void change(Integer id, Basket basket);

    void delete(Integer id);
}

