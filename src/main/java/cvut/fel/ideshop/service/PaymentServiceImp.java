package cvut.fel.ideshop.service;

import cvut.fel.ideshop.exceptions.NotFoundException;
import cvut.fel.ideshop.model.database.entity.Payment;
import cvut.fel.ideshop.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentServiceImp implements PaymentService {

    private final PaymentRepository paymentRepository;

    @Autowired
    public PaymentServiceImp(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    public Payment findById(Integer id) {

        if (id == null) {
            throw new RuntimeException("NULL_ID");
        }

        return paymentRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("PAYMENT_NOT_FOUND"));
    }

    @Override
    public Iterable<Payment> findAll() {
        return paymentRepository.findAll();
    }

    @Override
    public void make(Payment payment) {
        paymentRepository.save(payment);
    }

    @Override
    public void change(Integer id, Payment payment) {
        paymentRepository.deleteById(id);
        paymentRepository.save(payment);
    }

    @Override
    public void delete(Integer id) {
        paymentRepository.deleteById(id);
    }
}
