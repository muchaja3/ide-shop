package cvut.fel.ideshop.service;

import cvut.fel.ideshop.exceptions.FieldInvalidException;
import cvut.fel.ideshop.exceptions.NotFoundException;
import cvut.fel.ideshop.model.database.entity.Product;
import cvut.fel.ideshop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImp implements ProductService {
    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product findById(Integer id) {
        if (id == null) {
            throw new FieldInvalidException("NULL_ID");
        }
        return productRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("PRODUCT_NOT_FOUND"));
    }

    @Override
    public Iterable<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public void make(Product product) {
        productRepository.save(product);
    }

    @Override
    public void change(Integer id, Product product) {
        productRepository.deleteById(id);
        productRepository.save(product);
    }

    @Override
    public void delete(Integer id) {
        productRepository.deleteById(id);
    }
}
