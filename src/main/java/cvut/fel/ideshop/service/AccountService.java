package cvut.fel.ideshop.service;

import cvut.fel.ideshop.model.database.entity.Account;

public interface AccountService {
    Account findById(Integer id);

    Iterable<Account> findAll();

    void make(Account account);

    void change(Integer id, Account account);

    void delete(Integer id);
}
