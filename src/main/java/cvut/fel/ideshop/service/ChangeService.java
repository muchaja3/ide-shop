package cvut.fel.ideshop.service;

import cvut.fel.ideshop.model.database.entity.Change;

public interface ChangeService {
    Change findById(Integer id);

    Iterable<Change> findAll();

    void make(Change change);

    void change(Integer id, Change change);

    void delete(Integer id);
}
