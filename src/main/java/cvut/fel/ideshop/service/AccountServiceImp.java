package cvut.fel.ideshop.service;

import cvut.fel.ideshop.exceptions.NotFoundException;
import cvut.fel.ideshop.model.database.entity.Account;
import cvut.fel.ideshop.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AccountServiceImp implements AccountService {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountServiceImp(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Account findById(Integer id) {
        if (id == null) {
            throw new RuntimeException();
        }
        return accountRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("ACCOUNT_NOT_FOUND"));
    }

    @Override
    public Iterable<Account> findAll() {
        return accountRepository.findAll();
    }

    @Override
    public void make(Account account) {
        accountRepository.save(account);
    }

    @Override
    public void change(Integer id, Account account) {
        accountRepository.deleteById(id);
        accountRepository.save(account);
    }

    @Override
    public void delete(Integer id) {
        accountRepository.deleteById(id);
    }
}
