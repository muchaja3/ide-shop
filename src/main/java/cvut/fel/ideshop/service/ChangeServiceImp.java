package cvut.fel.ideshop.service;

import cvut.fel.ideshop.exceptions.NotFoundException;
import cvut.fel.ideshop.model.database.entity.Change;
import cvut.fel.ideshop.repository.ChangeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ChangeServiceImp implements ChangeService {
    private final ChangeRepository changeRepository;

    @Autowired
    public ChangeServiceImp(ChangeRepository changeRepository) {
        this.changeRepository = changeRepository;
    }

    public Change findById(Integer id) {
        if (id == null) {
            throw new RuntimeException();
        }
        return changeRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("CHANGE_NOT_FOUND"));
    }

    @Override
    public Iterable<Change> findAll() {
        return changeRepository.findAll();
    }

    @Override
    public void make(Change change) {
        changeRepository.save(change);
    }

    @Override
    public void change(Integer id, Change change) {
        changeRepository.deleteById(id);
        changeRepository.save(change);
    }

    @Override
    public void delete(Integer id) {
        changeRepository.deleteById(id);
    }
}
