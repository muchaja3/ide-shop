package cvut.fel.ideshop.service;

import cvut.fel.ideshop.exceptions.FieldInvalidException;
import cvut.fel.ideshop.exceptions.NotFoundException;
import cvut.fel.ideshop.model.database.entity.Basket;
import cvut.fel.ideshop.repository.BasketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BasketServiceImp implements BasketService {
    private final BasketRepository basketRepository;

    @Autowired
    public BasketServiceImp(BasketRepository basketRepository) {
        this.basketRepository = basketRepository;
    }

    public Basket findById(Integer id) {
        if (id == null) {
            throw new FieldInvalidException("NULL_ID");
        }
        return basketRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("BASKET_NOT_FOUND"));
    }

    @Override
    public Iterable<Basket> findAll() {
        return basketRepository.findAll();
    }

    @Override
    public void make(Basket basket) {
        basketRepository.save(basket);
    }

    @Override
    public void change(Integer id, Basket basket) {
        basketRepository.deleteById(id);
        basketRepository.save(basket);
    }

    @Override
    public void delete(Integer id) {
        basketRepository.deleteById(id);
    }
}
