package cvut.fel.ideshop.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractDTO implements Cloneable {

    private Long id;

    public AbstractDTO() {
    }

    public AbstractDTO(AbstractDTO dto) {
        id = dto.getId();
    }

    protected abstract AbstractDTO clone() throws CloneNotSupportedException;
}