package cvut.fel.ideshop.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ProductDTO extends AbstractDTO {
    private String description;
    private String name;
    private Double price;

    public ProductDTO(String description, String name, Double price) {
        this.description = description;
        this.name = name;
        this.price = price;
    }

    public ProductDTO(ProductDTO productDTO) {
        super(productDTO);
        this.description = productDTO.getDescription();
        this.name = productDTO.getName();
        this.price = productDTO.getPrice();
    }

    @Override
    public AbstractDTO clone() {
        return new ProductDTO((this));
    }
}
