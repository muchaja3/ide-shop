package cvut.fel.ideshop.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ChangeDTO extends AbstractDTO {

    private String description;
    private String name;
    private String price;

    public ChangeDTO(String description, String name, String price) {
        this.description = description;
        this.name = name;
        this.price = price;
    }

    public ChangeDTO(ChangeDTO changeDTO) {
        super(changeDTO);
        this.description = getDescription();
        this.name = getName();
        this.price = getPrice();
    }

    @Override
    protected AbstractDTO clone() throws CloneNotSupportedException {
        return new ChangeDTO(this);
    }
}
