package cvut.fel.ideshop.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AccountDTO extends AbstractDTO {

    private String email;
    private String name;
    private String password;

    public AccountDTO(String email, String name, String password) {
        this.email = email;
        this.name = name;
        this.password = password;
    }

    public AccountDTO(AccountDTO accountDTO) {
        super(accountDTO);
        this.email = accountDTO.getEmail();
        this.name = accountDTO.getName();
        this.password = accountDTO.getPassword();
    }

    @Override
    protected AbstractDTO clone() throws CloneNotSupportedException {
        return new AccountDTO(this);
    }
}
