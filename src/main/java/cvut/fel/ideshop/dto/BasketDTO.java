package cvut.fel.ideshop.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class BasketDTO extends AbstractDTO {
    public int amountOfProduct;
    public String price;

    public BasketDTO(BasketDTO basketDTO) {
        this.amountOfProduct = basketDTO.getAmountOfProduct();
        this.price = basketDTO.getPrice();
    }

    public BasketDTO(int amountOfProduct, String price) {
        this.amountOfProduct = amountOfProduct;
        this.price = price;
    }

    @Override
    protected AbstractDTO clone() throws CloneNotSupportedException {
        return null;
    }
}
