package cvut.fel.ideshop.dto;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
public class PaymentDTO {
    public Timestamp date;
    public String product;
    public String price;


    public PaymentDTO(Timestamp timestamp, String product, String price) {
        this.date = timestamp;
        this.product = product;
        this.price = price;
    }
}
