package cvut.fel.ideshop.dto;

import org.mapstruct.Mapper;
import cvut.fel.ideshop.model.database.entity.*;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@Component
public interface DTOMapper {

    // --- Account ---
    AccountDTO accountAndDTO(Account account);


    Account dtoAndAccount(AccountDTO accountDTO);

    // --- Basket ---
    BasketDTO basketAndDTO(Basket basket);

    Basket dtoAndBasket(BasketDTO basketDTO);

    // --- Change ---
    ChangeDTO changeAndDTO(Change change);

    Change dtoAndChange(ChangeDTO changeDTO);

    // --- Payment ---
    PaymentDTO paymentAndDTO(Payment payment);

    Payment dtoAndPayment(PaymentDTO paymentDTO);

    // --- Product ---
    ProductDTO productAndDTO(Product product);

    Product dtoAndProduct(ProductDTO productDTO);
}
