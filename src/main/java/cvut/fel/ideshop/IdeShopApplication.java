package cvut.fel.ideshop;

import cvut.fel.ideshop.model.database.entity.*;
import cvut.fel.ideshop.repository.AllEntity;
import cvut.fel.ideshop.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class IdeShopApplication implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(IdeShopApplication.class);


    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private AllEntity allEntity;

    public static void main(String[] args) {
        SpringApplication.run(IdeShopApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("StartApplication...");
        // Generator account
        Account account = GeneratorTemp.generateAccount();
        allEntity.saveAccount(account);
        System.out.println(account);
        // Generator basket
        Basket basket = GeneratorTemp.generateBasket();
        allEntity.saveBasket(basket);
        System.out.println(basket);
        // Generator change
        Change change = GeneratorTemp.generateChange();
        allEntity.saveChange(change);
        System.out.println(change);
        // Generator payment
        Payment payment = GeneratorTemp.generatePayment();
        allEntity.savePayment(payment);
        System.out.println(payment);
        // Generator products
        Product product1 = new Product();
        product1.setId(2);
        product1.setName("IntelliJ");
        product1.setDescription("HelloWorld");
        product1.setPrice("2312$");
        Product product2 = new Product();
        product2.setId(3);
        product2.setName("CLion");
        product2.setDescription("ByeWorld");
        product2.setPrice("2303$");
        Product product3 = new Product();
        product3.setId(4);
        product3.setName("PyCharm");
        product3.setDescription("SebekDontKillUsPls");
        product3.setPrice("1302$");
        allEntity.saveProduct(product1);
        allEntity.saveProduct(product2);
        allEntity.saveProduct(product3);
        productRepository.save(GeneratorTemp.generateProduct());
        // Generator role
        Role role = new Role();
        allEntity.saveRole(role);
        System.out.println(role);

        System.out.println("\nfindAll()");
        productRepository.findAll().forEach(System.out::println);

        System.out.println("\nfindById(2)");
        productRepository.findById(2).ifPresent(System.out::println);

        System.out.println("\nfindByName('Node')");
        productRepository.findByName("IntelliJ").forEach(System.out::println);
    }
}
