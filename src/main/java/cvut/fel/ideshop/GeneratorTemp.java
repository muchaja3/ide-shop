package cvut.fel.ideshop;

import cvut.fel.ideshop.model.database.entity.*;

import java.util.Random;

public class GeneratorTemp {
    private static final Random RAND = new Random();

    public static Account generateAccount() {
        final Account account = new Account("Jan", "Balicek");
        account.setPassword("******");
        account.setEmail("balicekispackage@balicek.bk");
        account.setId(Integer.parseInt(String.valueOf(RAND.nextInt())));
        return account;
    }

    public static Basket generateBasket() {
        final Basket basket = new Basket(2, "1000Kc");
        basket.setId(Integer.parseInt(String.valueOf(RAND.nextInt())));
        return basket;
    }

    public static Change generateChange() {
        final Change change = new Change("PyCharm", "100$", "JellyBeans");
        change.setId(Integer.parseInt(String.valueOf(RAND.nextInt())));
        return change;
    }

    public static Payment generatePayment() {
        final Payment payment = new Payment("IDontKnow", "IDE");
        payment.setId(Integer.parseInt(String.valueOf(RAND.nextInt())));
        return payment;
    }

    public static Product generateProduct() {
        final Product product = new Product("Java", "1231$", "JellyBeans");
        product.setId(Integer.parseInt(String.valueOf(RAND.nextInt())));
        return product;
    }

    public static Role generateRole() {
        final Role role = new Role("Admin");
        role.setId(String.valueOf(Integer.parseInt(String.valueOf(RAND.nextInt()))));
        return role;
    }

}
