package cvut.fel.ideshop.observer;

public interface Observer {
    void update();
}
