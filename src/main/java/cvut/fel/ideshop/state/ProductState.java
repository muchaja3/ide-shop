package cvut.fel.ideshop.state;

import cvut.fel.ideshop.model.database.entity.Product;
import cvut.fel.ideshop.model.database.entity.ProductIDE;

public interface ProductState {
    Product createProduct(int id, ProductIDE productIde);
}
