package cvut.fel.ideshop.repository;

import cvut.fel.ideshop.model.database.entity.Change;
import cvut.fel.ideshop.model.database.entity.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ChangeRepository extends CrudRepository<Change, Integer> {

    List<Change> findById(int id);

    <S extends Product> S save(S entity);

    void deleteById(Integer id);
}
