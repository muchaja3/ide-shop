package cvut.fel.ideshop.repository;

import cvut.fel.ideshop.model.database.entity.Product;
import cvut.fel.ideshop.model.database.entity.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RoleRepository extends CrudRepository<Role, Integer> {
    List<Role> findById(List<Integer> ids);

    <S extends Product> S save(S entity);

    void deleteById(Integer id);
}
