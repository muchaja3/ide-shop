package cvut.fel.ideshop.repository;

import cvut.fel.ideshop.model.database.entity.Payment;
import cvut.fel.ideshop.model.database.entity.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PaymentRepository extends CrudRepository<Payment, Integer> {

    boolean existsById(Integer id);

    List<Payment> findById(List<Integer> ids);

    <S extends Product> S save(S entity);

    void deleteById(Integer id);
}
