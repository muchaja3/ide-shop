package cvut.fel.ideshop.repository;

import cvut.fel.ideshop.model.database.entity.Basket;
import cvut.fel.ideshop.model.database.entity.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BasketRepository extends CrudRepository<Basket, Integer> {
    List<Basket> findById(List<Integer> ids);

    <S extends Product> S save(S entity);

    void deleteById(Integer id);
}

