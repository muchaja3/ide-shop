package cvut.fel.ideshop.repository;

import cvut.fel.ideshop.model.database.entity.Account;
import cvut.fel.ideshop.model.database.entity.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends CrudRepository<Account, Integer> {

    List<Account> findById(int id);

    <S extends Product> S save(S entity);

    void deleteById(Integer id);
}
