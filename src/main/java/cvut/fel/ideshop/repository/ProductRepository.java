package cvut.fel.ideshop.repository;

import cvut.fel.ideshop.model.database.entity.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product, Integer> {
    List<Product> findById(List<Integer> ids);

    List<Product> findByName(String name);

    <S extends Product> S save(S entity);

    void deleteById(Integer id);
}
