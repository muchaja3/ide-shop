package cvut.fel.ideshop.repository;

import cvut.fel.ideshop.model.database.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AllEntity {

    private final AccountRepository accountRepository;
    private final BasketRepository basketRepository;
    private final ChangeRepository changeRepository;
    private final PaymentRepository paymentRepository;
    private final ProductRepository productRepository;
    private final RoleRepository roleRepository;


    @Autowired
    public AllEntity(AccountRepository accountRepository,
                     BasketRepository basketRepository,
                     ChangeRepository changeRepository,
                     PaymentRepository paymentRepository,
                     ProductRepository productRepository,
                     RoleRepository roleRepository) {
        this.accountRepository = accountRepository;
        this.basketRepository = basketRepository;
        this.changeRepository = changeRepository;
        this.paymentRepository = paymentRepository;
        this.productRepository = productRepository;
        this.roleRepository = roleRepository;
    }

    public Account saveAccount(Account entity) {
        return accountRepository.save(entity);
    }

    public Basket saveBasket(Basket entity) {
        return basketRepository.save(entity);
    }

    public Change saveChange(Change entity) {
        return changeRepository.save(entity);
    }

    public Payment savePayment(Payment entity) {
        return paymentRepository.save(entity);
    }

    public Product saveProduct(Product entity) {
        return productRepository.save(entity);
    }

    public Role saveRole(Role entity) {
        return roleRepository.save(entity);
    }

}
