package cvut.fel.ideshop.model.database.entity;

import cvut.fel.ideshop.observer.Observer;

public class Admin extends Account implements Observer {

    public void update() {
        System.out.println("Admin has been notified");
    }
}
