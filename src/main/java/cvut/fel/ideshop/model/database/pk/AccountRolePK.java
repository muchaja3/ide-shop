package cvut.fel.ideshop.model.database.pk;

import javax.persistence.Column;
import java.io.Serializable;

public abstract class AccountRolePK implements Serializable {

    @Column(name = "account_id")
    private int accountId;

    @Column(name = "role_id")
    private String roleId;
}
