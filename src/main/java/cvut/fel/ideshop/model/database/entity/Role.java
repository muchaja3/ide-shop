package cvut.fel.ideshop.model.database.entity;

import cvut.fel.ideshop.model.database.relation.AccountRole;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Role")
public class Role {

    @Id
    @Column(name = "id")
    private String id;
    @Basic
    @Column(name = "role_type")
    private String roleType;
    @ManyToMany(mappedBy = "connectByAccountId")
    private List<AccountRole> accountRolesById;

    public Role() {
    }

    public Role(String roleType) {
        this();
        setRoleType(roleType);
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public List<AccountRole> getAccountRolesById() {
        return accountRolesById;
    }

    public void setAccountRolesById(List<AccountRole> accountRolesById) {
        this.accountRolesById = accountRolesById;
    }
}
