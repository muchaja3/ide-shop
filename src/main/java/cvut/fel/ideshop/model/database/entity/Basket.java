package cvut.fel.ideshop.model.database.entity;

import cvut.fel.ideshop.model.database.relation.AccountBasket;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Basket")

@NamedQueries({
        @NamedQuery(name = "Basket.findById", query = "SELECT b FROM Basket b WHERE b.id = :id"),
})


public class Basket {

    @Id
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "products_amount")
    private int productsAmount;
    @Basic
    @Column(name = "total_price")
    private String totalPrice;
    @OneToMany(mappedBy = "connectById")
    private List<AccountBasket> accountBasketsById;

    public Basket() {
    }

    public Basket(int productsAmount, String totalPrice) {
        this();
        setProductsAmount(productsAmount);
        setTotalPrice(totalPrice);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductsAmount() {
        return productsAmount;
    }

    public void setProductsAmount(int productsAmount) {
        this.productsAmount = productsAmount;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<AccountBasket> getAccountBasketsById() {
        return accountBasketsById;
    }

    public void setAccountBasketsById(List<AccountBasket> accountBasketsById) {
        this.accountBasketsById = accountBasketsById;
    }
}
