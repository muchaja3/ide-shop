package cvut.fel.ideshop.model.database.pk;

import javax.persistence.Column;
import java.io.Serializable;

public abstract class AccountBasketPK implements Serializable {

    @Column(name = "account_id")
    private int accountId;

    @Column(name = "basket_id")
    private int basketId;
}
