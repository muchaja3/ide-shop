package cvut.fel.ideshop.model.database.relation;

import cvut.fel.ideshop.model.database.entity.Basket;
import cvut.fel.ideshop.model.database.pk.BasketProductPK;

import javax.persistence.*;

@Entity
@IdClass(BasketProductPK.class)
@Table(name = "Basket_Product", schema = "nss-schema", catalog = "rastvdmy")
public class BasketProduct {
    @Id
    @Column(name = "product_id")
    private int productId;
    @Id
    @Column(name = "basket_id")
    private int basketId;
    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "id", nullable = false,
            insertable = false, updatable = false)
    private Basket connectByBasketId;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getBasketId() {
        return basketId;
    }

    public void setBasketId(int basketId) {
        this.basketId = basketId;
    }

    public Basket getBasketByBasketId() {
        return connectByBasketId;
    }

    public void setBasketByBasketId(Basket basketByBasketId) {
        this.connectByBasketId = basketByBasketId;
    }
}
