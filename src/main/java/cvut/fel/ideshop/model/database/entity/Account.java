package cvut.fel.ideshop.model.database.entity;

import cvut.fel.ideshop.model.database.relation.AccountRole;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Account")
@NamedQueries({
        @NamedQuery(name = "Account.findById", query = "SELECT a FROM Account a WHERE a.id = :id"),
})
public class Account {

    @Id
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "email")
    private String email;
    @Basic
    @Column(name = "firstname")
    private String firstname;
    @Basic
    @Column(name = "lastname")
    private String lastname;
    @Basic
    @Column(name = "password")
    private String password;

    @ManyToMany(mappedBy = "connectByAccountId")
    private List<AccountRole> accountRolesById;

    public Account() {

    }

    public Account(String firstname, String lastname) {
        this();
        setFirstname(firstname);
        setLastname(lastname);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<AccountRole> getAccountRolesById() {
        return accountRolesById;
    }

    public void setAccountRolesById(List<AccountRole> accountRolesById) {
        this.accountRolesById = accountRolesById;
    }
}
