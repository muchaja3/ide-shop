package cvut.fel.ideshop.model.database.entity;

public class ProductIDE {
    public String name;
    public String price;
    public String description;


    public ProductIDE(String name, String price, String description) {
        this.name = name;
        this.price = price;
        this.description = description;
    }
}
