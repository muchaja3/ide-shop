package cvut.fel.ideshop.model.database.relation;

import cvut.fel.ideshop.model.database.entity.Basket;
import cvut.fel.ideshop.model.database.pk.BasketPaymentPK;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@IdClass(BasketPaymentPK.class)
@Table(name = "Basket_Payment", schema = "nss-schema", catalog = "rastvdmy")
public class BasketPayment implements Serializable {
    @Id
    @Column(name = "basket_id")
    private int basketId;
    @Id
    @Column(name = "payment_id")
    private int paymentId;
    @ManyToOne
    @JoinColumn(name = "basket_id", referencedColumnName = "id", nullable = false,
            insertable = false, updatable = false)
    private Basket connectByPaymentId;

    public int getBasketId() {
        return basketId;
    }

    public void setBasketId(int basketId) {
        this.basketId = basketId;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public Basket getBasketByBasketId() {
        return connectByPaymentId;
    }

    public void setBasketByBasketId(Basket basketByBasketId) {
        this.connectByPaymentId = basketByBasketId;
    }
}
