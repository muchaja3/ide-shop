package cvut.fel.ideshop.model.database.entity;

import cvut.fel.ideshop.model.database.relation.BasketProduct;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Product")
@NamedQueries({
        @NamedQuery(name = "Product.findByName", query = "SELECT p FROM Product p WHERE p.name = :name"),
})
public class Product {
    @Id
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "price")
    private String price;
    @Basic
    @Column(name = "description")
    private String description;
    @OneToMany(mappedBy = "connectByBasketId")
    private List<BasketProduct> basketProductsById;

    public Product() {

    }

    public Product(String name, String price, String description) {
        this();
        setName(name);
        setPrice(price);
        setDescription(description);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<BasketProduct> getBasketProductsById() {
        return basketProductsById;
    }

    public void setBasketProductsById(List<BasketProduct> basketProductsById) {
        this.basketProductsById = basketProductsById;
    }
}
