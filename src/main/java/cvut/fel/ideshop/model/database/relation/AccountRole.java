package cvut.fel.ideshop.model.database.relation;

import cvut.fel.ideshop.model.database.entity.Account;
import cvut.fel.ideshop.model.database.pk.AccountRolePK;

import javax.persistence.*;
import java.util.List;

@Entity
@IdClass(AccountRolePK.class)
@Table(name = "Account_Role", schema = "nss-schema", catalog = "rastvdmy")
public class AccountRole {
    @Id
    @Column(name = "account_id")
    private int accountId;
    @Id
    @Column(name = "role_id")
    private String roleId;
    @ManyToMany
    @JoinColumns({@JoinColumn(name = "account_id", referencedColumnName = "id", nullable = false),
            @JoinColumn(name = "role_id", referencedColumnName = "id", nullable = false)})

    private List<Account> connectByAccountId;

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public List<Account> getAccountByAccountId() {
        return connectByAccountId;
    }

    public void setAccountByAccountId(List<Account> accountByAccountId) {
        this.connectByAccountId = accountByAccountId;
    }
}