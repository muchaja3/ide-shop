package cvut.fel.ideshop.model.database.relation;

import cvut.fel.ideshop.model.database.entity.Account;
import cvut.fel.ideshop.model.database.pk.AccountBasketPK;

import javax.persistence.*;

@Entity
@IdClass(AccountBasketPK.class)
@Table(name = "Account_Basket", schema = "nss-schema", catalog = "rastvdmy")
public class AccountBasket {
    @Id
    @Column(name = "account_id")
    private int accountId;
    @Id
    @Column(name = "basket_id")
    private int basketId;

    @ManyToOne
    @JoinColumn(name = "basket_id", referencedColumnName = "id", nullable = false,
            insertable = false, updatable = false)
    private Account connectById;

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getBasketId() {
        return basketId;
    }

    public void setBasketId(int basketId) {
        this.basketId = basketId;
    }

    public Account getAccountByAccountId() {
        return connectById;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.connectById = accountByAccountId;
    }
}
