package cvut.fel.ideshop.model.database.pk;

import javax.persistence.Column;
import java.io.Serializable;

public abstract class BasketProductPK implements Serializable {

    @Column(name = "product_id")
    private int productId;

    @Column(name = "basket_id")
    private int basketId;
}
