package cvut.fel.ideshop.model.database.relation;

import cvut.fel.ideshop.model.database.entity.Change;
import cvut.fel.ideshop.model.database.entity.Product;
import cvut.fel.ideshop.model.database.pk.ProductChangePK;

import javax.persistence.*;

@Entity
@IdClass(ProductChangePK.class)
@Table(name = "Product_Change", schema = "nss-schema", catalog = "rastvdmy")
public class ProductChange {
    @Id
    @Column(name = "change_id")
    private int changeId;
    @Id
    @Column(name = "product_id")
    private int productId;
    @ManyToOne
    @JoinColumn(name = "change_id", referencedColumnName = "id", nullable = false,
            insertable = false, updatable = false)
    private Change changeByChangeId;
    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "id", nullable = false,
            insertable = false, updatable = false)
    private Product productByProductId;

    public int getChangeId() {
        return changeId;
    }

    public void setChangeId(int changeId) {
        this.changeId = changeId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public Change getChangeByChangeId() {
        return changeByChangeId;
    }

    public void setChangeByChangeId(Change changeByChangeId) {
        this.changeByChangeId = changeByChangeId;
    }

    public Product getProductByProductId() {
        return productByProductId;
    }

    public void setProductByProductId(Product productByProductId) {
        this.productByProductId = productByProductId;
    }
}
