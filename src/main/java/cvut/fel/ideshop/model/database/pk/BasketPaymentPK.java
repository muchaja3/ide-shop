package cvut.fel.ideshop.model.database.pk;

import javax.persistence.Column;
import java.io.Serializable;

public abstract class BasketPaymentPK implements Serializable {

    @Column(name = "basket_id")
    private int basketId;

    @Column(name = "payment_id")
    private int paymentId;
}
