package cvut.fel.ideshop.model.database.entity;

import cvut.fel.ideshop.model.database.relation.BasketPayment;

import javax.persistence.*;

@Entity
@Table(name = "Payment")
@NamedQueries({
        @NamedQuery(name = "Payment.findById", query = "SELECT p FROM Payment p WHERE p.id = :id"),
})
public class Payment {
    @Id
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "info")
    private String info;
    @Basic
    @Column(name = "type")
    private String type;
    @OneToOne(mappedBy = "connectByPaymentId")
    private BasketPayment basketPaymentsById;

    public Payment() {
    }

    public Payment(String info, String type) {
        this();
        setInfo(info);
        setType(type);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BasketPayment getBasketPaymentsById() {
        return basketPaymentsById;
    }

    public void setBasketPaymentsById(BasketPayment basketPaymentsById) {
        this.basketPaymentsById = basketPaymentsById;
    }
}
