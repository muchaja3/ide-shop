package cvut.fel.ideshop.model.database.entity;

import javax.persistence.*;

@Entity
@Table(name = "Change")
@NamedQueries({
        @NamedQuery(name = "Change.findById", query = "SELECT c FROM Change c WHERE c.id = :id"),
})
public class Change {
    @Id
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "old_name")
    private String oldName;
    @Basic
    @Column(name = "old_price")
    private String oldPrice;
    @Basic
    @Column(name = "old_description")
    private String oldDescription;

    public Change() {
    }

    public Change(String oldName, String oldPrice, String oldDescription) {
        this();
        setOldName(oldName);
        setOldPrice(oldPrice);
        setOldDescription(oldDescription);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOldName() {
        return oldName;
    }

    public void setOldName(String oldName) {
        this.oldName = oldName;
    }

    public String getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(String oldPrice) {
        this.oldPrice = oldPrice;
    }

    public String getOldDescription() {
        return oldDescription;
    }

    public void setOldDescription(String oldDescription) {
        this.oldDescription = oldDescription;
    }
}
