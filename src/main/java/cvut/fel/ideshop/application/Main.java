package cvut.fel.ideshop.application;

import cvut.fel.ideshop.model.database.entity.*;

import java.util.ArrayList;
import java.util.List;


public class Main {
    public static Executor ed;

    public static void main(String[] args) {
        ed = new Executor();
        Admin admin = new Admin();
        List<ProductIDE> productIdes = new ArrayList<>();
        ed.attach(admin);
        // ---- Create account ----
        Account account1 = ed.createAccount(
                1,
                "testuser01@gmail.com",
                "Test",
                "User",
                "JavaIsMyLove");

        Account account2 = ed.createAccount(
                2,
                "testuser02@seznam.com",
                "Test2",
                "User2",
                "IReallyJavaLover");

        Account account3 = ed.createAccount(
                3,
                "testuser03@icloud.com",
                "Test3",
                "User3",
                "HelloWorld");

        // ---- Operations with account ----
        Account findAcc = ed.findAccount(
                3,
                "testuser03@icloud.com",
                "Test3",
                "User3",
                "HelloWorld");

        if (findAcc == null) {
            System.err.println("This account does not exist!");
        } else {
            System.out.println("Account exists in database");
        }

        ed.updateAccount(
                        account1,
                        "testeruserer01@fel.cvut.cz",
                        "Tester",
                        "Userer",
                        "Javonius")
                .updateAccount(
                        account2,
                        "testeruserer02@gmail.com",
                        "Testerorer",
                        "Userorer",
                        "Javus");

        ed.removeAccount(account3).removeAccount(account2);

        // ----- Create basket -----
        Basket basket1 = ed.createBasket(1, 10, "123$");
        Basket basket2 = ed.createBasket(2, 5, "10$");
        Basket basket3 = ed.createBasket(3, 1, "305Kč");

        // ---- Operations with account ----
        Basket findBask = ed.findBasket(1, 10, "123$");

        if (findBask == null) {
            System.err.println("This basket does not exist!");
        } else {
            System.out.println("Basket exists in database");
        }

        ed.updateBasket(basket1, 100, "1100$")
                .updateBasket(basket2, 14, "304$");

        ed.removeBasket(basket2).removeBasket(basket3);

        // ----- Create change -----
        Change change1 = ed.createChange(1, "C", "123$", "Why you are using it?");
        Change change2 = ed.createChange(2, "Java", "3100$", "Good choice");
        Change change3 = ed.createChange(3, "Python", "1200$", "Popular language");

        // ---- Operations with change ----
        Change findChange = ed.findChange(1, "C", "123$", "Why you are using it?");

        if (findChange == null) {
            System.err.println("This change does not exist!");
        } else {
            System.out.println("Change exists in database");
        }

        ed.updateChange(change1, "C++", "100$", "Yes")
                .updateChange(change2, "JavaScript", "330Kč", "Interesting");

        ed.removeChange(change1).removeChange(change3);

        // ----- Create payment -----
        Payment payment1 = ed.createPayment(1, "Here", "Java");
        Payment payment2 = ed.createPayment(2, "What", "Scratch");
        Payment payment3 = ed.createPayment(3, "Why", "Python");

        // ---- Operations with payment ----
        Payment findPayment = ed.findPayment(1, "Here", "Java");

        if (findPayment == null) {
            System.err.println("This payment does not exist!");
        } else {
            System.out.println("Payment exists in database");
        }

        ed.updatePayment(payment1, "There", "Intellij IDEA")
                .updatePayment(payment2, "How", "CLion");

        ed.removePayment(payment2).removePayment(payment3);

        // ----- Create product -----

        ProductIDE productIde1 = new ProductIDE("Intellij IDEA", "1223$", "Well, u know enough");
        productIdes.add(productIde1);
        ProductIDE productIde2 = new ProductIDE("CLion", "1445$", "Interesting thing");
        productIdes.add(productIde2);
        ProductIDE productIde3 = new ProductIDE("PyCharm", "1322Kč", "Best solution");
        productIdes.add(productIde3);

        List<Product> products = new ArrayList<>();
        Product product1 = ed.createProduct(1, productIdes.get(0));
        products.add(product1);
        Product product2 = ed.createProduct(2, productIdes.get(1));
        products.add(product2);
        Product product3 = ed.createProduct(3, productIdes.get(2));
        products.add(product3);
        ed.initIDE(products);

        // ---- Operations with product ----
        Product findProduct = ed.findProduct(1, productIde1);
        if (findProduct == null) {
            System.err.println("This product does not exist!");
        } else {
            System.out.println("Product exists in database");
        }
        ProductIDE updateProductIDE1 = new ProductIDE("Intellij IDEA Ultimate", "200$", "Well, use me well");
        ProductIDE updateProductIDE3 = new ProductIDE("PyCharm", "1232$", "The best language for fast boi");
        ed.updateProduct(product1, updateProductIDE1)
                .updateProduct(product3, updateProductIDE3);

        ed.removeProduct(product2).removeProduct(product3);
        // ----- Create role -----
        Role role1 = ed.createRole("Dev", "D");
        Role role2 = ed.createRole("Guest", "W");
        Role role3 = ed.createRole("User", "Q");

        // ---- Operations with role ----
        Role findRole = ed.findRole("Dev", "D");
        if (findRole == null) {
            System.err.println("This role does not exist!");
        } else {
            System.out.println("Role exists in database");
        }
        ed.updateRole(role1, "G").updateRole(role3, "U");
        ed.removeRole(role2).removeRole(role3);

        ed.close();
    }
}
