package cvut.fel.ideshop.application;

import cvut.fel.ideshop.model.database.entity.*;
import cvut.fel.ideshop.observer.Observer;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Executor {
    private final List<Observer> observers = new ArrayList<>();

    private final HashMap<Integer, Product> products = new HashMap<>();

    private static EntityManager em;
    private static EntityManagerFactory emf;
    private static EntityTransaction et;


    public Executor() {
        emf = Persistence.createEntityManagerFactory("database");
        em = emf.createEntityManager();
    }

    public void close() {
        em.close();
        emf.close();
    }

    public void attach(Observer observer) {
        observers.add(observer);
    }

    public void notifyAdmin() {
        for (Observer observer : observers) {
            observer.update();
        }
    }

    // -------- Account --------
    public Account createAccount(int id, String email, String firstname, String lastname, String password) {
        et = em.getTransaction();
        et.begin();
        Account account = new Account();
        account.setId(id);
        account.setEmail(email);
        account.setFirstname(firstname);
        account.setLastname(lastname);
        account.setPassword(password);
        em.persist(account);
        et.commit();
        System.out.println("Add Account{ " +
                "id='" + id + '\'' +
                ", email='" + email + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", password='" + password + '\'' +
                "}");

        notifyAdmin();
        return account;
    }

    public Account findAccount(int id, String email, String firstname, String lastname, String password) {
        TypedQuery<Account> query = em.createQuery("SELECT b FROM Account b WHERE " +
                "b.id = :id " +
                "and b.email = :email  " +
                "and  b.firstname = :firstname" +
                " and b.lastname = :lastname" +
                " and b.password = :age", Account.class);
        query.setParameter("id", id);
        query.setParameter("email", email);
        query.setParameter("firstname", firstname);
        query.setParameter("lastname", lastname);
        query.setParameter("age", password);
        return query.getSingleResult();
    }

    public Executor updateAccount(Account account, String email, String firstname, String lastname, String password) {
        et = em.getTransaction();
        et.begin();
        account.setEmail(email);
        account.setFirstname(firstname);
        account.setLastname(lastname);
        account.setPassword(password);
        em.persist(account);
        et.commit();
        System.out.println("Update Account{ " +
                "email='" + email + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", password='" + password + '\'' +
                "}");
        return this;
    }

    public Executor removeAccount(Account account) {
        et = em.getTransaction();
        et.begin();
        em.remove(account);
        em.flush();
        et.commit();
        System.out.println("Remove Account (" + account.toString() + ")");
        return this;
    }

    // -------- Basket --------
    public Basket createBasket(int id, int productsAmount, String totalPrice) {
        et = em.getTransaction();
        et.begin();
        Basket basket = new Basket();
        basket.setId(id);
        basket.setProductsAmount(productsAmount);
        basket.setTotalPrice(totalPrice);
        em.persist(basket);
        et.commit();
        System.out.println("Add Basket{ " +
                "id='" + id + '\'' +
                ", products amount='" + productsAmount + '\'' +
                ", total price='" + totalPrice + '\'' +
                "}");
        return basket;
    }

    public Basket findBasket(int id, int productsAmount, String totalPrice) {
        TypedQuery<Basket> query = em.createQuery("SELECT b FROM Basket b WHERE " +
                "b.id = :id " +
                "and b.productsAmount = :products_amount  " +
                "and  b.totalPrice = :total_price", Basket.class);
        query.setParameter("id", id);
        query.setParameter("products_amount", productsAmount);
        query.setParameter("total_price", totalPrice);
        return query.getSingleResult();
    }

    public Executor updateBasket(Basket basket, int productsAmount, String totalPrice) {
        et = em.getTransaction();
        et.begin();
        basket.setProductsAmount(productsAmount);
        basket.setTotalPrice(totalPrice);
        em.persist(basket);
        et.commit();
        System.out.println("Update Basket{ " +
                "products amount='" + productsAmount + '\'' +
                ", total price='" + totalPrice + '\'' +
                "}");
        return this;
    }

    public Executor removeBasket(Basket basket) {
        et = em.getTransaction();
        et.begin();
        em.remove(basket);
        em.flush();
        et.commit();
        System.out.println("Remove Basket (" + basket.toString() + ")");
        return this;
    }

    // -------- Change --------
    public Change createChange(int id, String oldName, String oldPrice, String oldDescription) {
        et = em.getTransaction();
        et.begin();
        Change change = new Change();
        change.setId(id);
        change.setOldName(oldName);
        change.setOldPrice(oldPrice);
        change.setOldDescription(oldDescription);
        em.persist(change);
        et.commit();
        System.out.println("Add Change{ " +
                "id='" + id + '\'' +
                ", old name='" + oldName + '\'' +
                ", old price='" + oldPrice + '\'' +
                ", old description='" + oldDescription + '\'' +
                "}");
        return change;
    }

    public Change findChange(int id, String oldName, String oldPrice, String oldDescription) {
        TypedQuery<Change> query = em.createQuery("SELECT b FROM Change b WHERE " +
                "b.id = :id  " +
                "and b.oldName = :old_name " +
                "and b.oldPrice = :old_price " +
                "and b.oldDescription = :old_description", Change.class);
        query.setParameter("id", id);
        query.setParameter("old_name", oldName);
        query.setParameter("old_price", oldPrice);
        query.setParameter("old_description", oldDescription);
        return query.getSingleResult();
    }

    public Executor updateChange(Change change, String oldName, String oldPrice, String oldDescription) {
        et = em.getTransaction();
        et.begin();
        change.setOldName(oldName);
        change.setOldPrice(oldPrice);
        change.setOldDescription(oldDescription);
        em.persist(change);
        et.commit();
        System.out.println("Update Change{ " +
                "old name='" + oldName + '\'' +
                ", old price='" + oldPrice + '\'' +
                ", old description='" + oldDescription + '\'' +
                "}");
        return this;
    }

    public Executor removeChange(Change change) {
        et = em.getTransaction();
        et.begin();
        em.remove(change);
        em.flush();
        et.commit();
        System.out.println("Remove Change (" + change.toString() + ")");
        return this;
    }

    // -------- Payment --------
    public Payment createPayment(int id, String info, String type) {
        et = em.getTransaction();
        et.begin();
        Payment payment = new Payment();
        payment.setId(id);
        payment.setInfo(info);
        payment.setType(type);
        em.persist(payment);
        et.commit();
        System.out.println("Add Payment{ " +
                "id='" + id + '\'' +
                ", info='" + info + '\'' +
                ", type='" + type + '\'' +
                "}");
        return payment;
    }

    public Payment findPayment(int id, String info, String type) {
        TypedQuery<Payment> query = em.createQuery("SELECT b FROM Payment b WHERE " +
                "b.id = :id  " +
                "and b.info = :info " +
                "and b.type = :type ", Payment.class);
        query.setParameter("id", id);
        query.setParameter("info", info);
        query.setParameter("type", type);
        return query.getSingleResult();
    }

    public Executor updatePayment(Payment payment, String info, String type) {
        et = em.getTransaction();
        et.begin();
        payment.setInfo(info);
        payment.setType(type);
        em.persist(payment);
        et.commit();
        System.out.println("Update Payment{ " +
                "info='" + info + '\'' +
                ", type='" + type + '\'' +
                "}");
        return this;
    }

    public Executor removePayment(Payment payment) {
        et = em.getTransaction();
        et.begin();
        em.remove(payment);
        em.flush();
        et.commit();
        System.out.println("Remove Payment (" + payment.toString() + ")");
        return this;
    }

    // -------- Product --------
    public Product createProduct(int id, ProductIDE productIde) {
        EntityTransaction et = em.getTransaction();
        et.begin();
        Product product = new Product();
        product.setId(id);
        product.setName(productIde.name);
        product.setPrice(productIde.price);
        product.setDescription(productIde.description);
        em.persist(product);
        et.commit();
        System.out.println("Add Product{ " +
                "id='" + id + '\'' +
                "name='" + productIde.name + '\'' +
                ", price='" + productIde.price + '\'' +
                ", description='" + productIde.description + '\'' +
                "}");
        products.put(id, product);
        System.out.println(products.size());
        return product;
    }

    public void initIDE(List<Product> products) {
        for (Product product : products) {
            System.out.println("Product: " + product.getName() + " has been added to the store");
        }
    }

    public Product findProduct(int id, ProductIDE productIde) {
        TypedQuery<Product> query = em.createQuery("SELECT b FROM Product b WHERE " +
                "b.id = :id  " +
                "and b.name = :name " +
                "and b.price = :price " +
                "and b.description = :description ", Product.class);
        query.setParameter("id", id);
        query.setParameter("name", productIde.name);
        query.setParameter("price", productIde.price);
        query.setParameter("description", productIde.description);
        return query.getSingleResult();
    }

    public Executor updateProduct(Product product, ProductIDE productIde) {
        et = em.getTransaction();
        et.begin();
        product.setName(productIde.name);
        product.setPrice(productIde.price);
        product.setDescription(productIde.description);
        em.persist(product);
        et.commit();
        System.out.println("Update Product{ " +
                "name='" + productIde.name + '\'' +
                ", price='" + productIde.price + '\'' +
                ", description='" + productIde.description + '\'' +
                "}");
        return this;
    }

    public Executor removeProduct(Product product) {
        EntityTransaction et = em.getTransaction();
        et.begin();
        em.remove(product);
        em.flush();
        et.commit();
        System.out.println("Remove Product (" + product.toString() + ")");
        products.remove(product.getId());
        System.out.println("Deleted products" + products.size());
        return this;
    }

    // -------- Role --------
    public Role createRole(String id, String roleType) {
        et = em.getTransaction();
        et.begin();
        Role role = new Role();
        role.setId(id);
        role.setRoleType(roleType);
        em.persist(role);
        et.commit();
        System.out.println("Add Role{ " +
                "id='" + id + '\'' +
                "role Type='" + roleType + '\'' +
                "}");
        return role;
    }

    public Role findRole(String id, String roleType) {
        TypedQuery<Role> query = em.createQuery("SELECT b FROM Role b WHERE " +
                "b.id = :id  " +
                "and b.roleType = :role_type ", Role.class);
        query.setParameter("id", id);
        query.setParameter("role_type", roleType);
        return query.getSingleResult();
    }

    public Executor updateRole(Role role, String roleType) {
        et = em.getTransaction();
        et.begin();
        role.setRoleType(roleType);
        em.persist(role);
        et.commit();
        System.out.println("Update Role{ " + "role Type='" + roleType + '\'' + "}");
        return this;
    }

    public Executor removeRole(Role role) {
        et = em.getTransaction();
        et.begin();
        em.remove(role);
        em.flush();
        et.commit();
        System.out.println("Remove Role (" + role.toString() + ")");
        return this;
    }
}
