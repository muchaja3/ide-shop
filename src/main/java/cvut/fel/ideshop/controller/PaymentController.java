package cvut.fel.ideshop.controller;

import cvut.fel.ideshop.dto.DTOMapper;
import cvut.fel.ideshop.dto.PaymentDTO;
import cvut.fel.ideshop.model.database.entity.Payment;
import cvut.fel.ideshop.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.logging.Logger;

@RestController
public class PaymentController {

    public static final Logger LOGGER = Logger.getGlobal();

    private final PaymentService paymentService;
    private final DTOMapper dtoMapper;

    @Autowired
    public PaymentController(PaymentService paymentService, DTOMapper dtoMapper) {
        this.paymentService = paymentService;
        this.dtoMapper = dtoMapper;
    }

    @GetMapping("/payment/{id}")
    public ResponseEntity<PaymentDTO> getAccountById(@PathVariable Integer id) {
        Payment payment = paymentService.findById(id);
        LOGGER.info("Get mapping payment id has been initialized\n");
        return ResponseEntity.ok(dtoMapper.paymentAndDTO(payment));
    }

    @GetMapping("/payments")
    public ResponseEntity<ArrayList<PaymentDTO>> getPayments() {
        Iterable<Payment> payments = paymentService.findAll();

        ArrayList<PaymentDTO> paymentDTOs = new ArrayList<>();
        for (Payment ch : payments) {
            paymentDTOs.add(dtoMapper.paymentAndDTO(ch));
        }
        LOGGER.info("Get mapping payments has been initialized\n");
        return ResponseEntity.ok(paymentDTOs);
    }

    @PostMapping("/newPayment")
    public void newPayment(@RequestParam("payment") RequestEntity<PaymentDTO> payment) {
        LOGGER.info("Post mapping new payment has been initialized\n");
        paymentService.make(dtoMapper.dtoAndPayment(payment.getBody()));
    }

    @PutMapping("/payment/{id}")
    public void updatePayment(@PathVariable Integer id, @RequestParam("payment") RequestEntity<PaymentDTO> payment) {
        LOGGER.info("Put mapping payment id has been initialized\n");
        paymentService.change(id, dtoMapper.dtoAndPayment(payment.getBody()));
    }

    @DeleteMapping("/payment/{id}")
    public void deletePayment(@PathVariable Integer id) {
        LOGGER.info("Delete mapping payment id has been initialized\n");
        paymentService.delete(id);
    }
}
