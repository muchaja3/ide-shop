package cvut.fel.ideshop.controller;

import cvut.fel.ideshop.dto.AccountDTO;
import cvut.fel.ideshop.dto.DTOMapper;
import cvut.fel.ideshop.model.database.entity.Account;
import cvut.fel.ideshop.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.logging.Logger;

@RestController
public class AccountController {

    public static final Logger LOGGER = Logger.getGlobal();

    private final AccountService accountService;
    private final DTOMapper dtoMapper;


    @Autowired
    public AccountController(AccountService accountService, DTOMapper dtoMapper) {
        this.accountService = accountService;
        this.dtoMapper = dtoMapper;
    }

    @GetMapping("/account/{id}")
    public ResponseEntity<AccountDTO> getAccountById(@PathVariable Integer id) {
        Account account = accountService.findById(id);
        LOGGER.info("Get mapping account id has been initialized\n");
        return ResponseEntity.ok(dtoMapper.accountAndDTO(account));
    }

    @GetMapping("/accounts")
    public ResponseEntity<ArrayList<AccountDTO>> getAccounts() {
        Iterable<Account> accounts = accountService.findAll();

        ArrayList<AccountDTO> accountDTOs = new ArrayList<>();
        for (Account ac : accounts) {
            accountDTOs.add(dtoMapper.accountAndDTO(ac));
        }
        LOGGER.info("Get mapping accounts has been initialized\n");
        return ResponseEntity.ok(accountDTOs);
    }

    @PostMapping("/newAccount")
    public void newAccount(@RequestParam("account") RequestEntity<AccountDTO> account) {
        accountService.make(dtoMapper.dtoAndAccount(account.getBody()));
        LOGGER.info("Post mapping new account has been initialized\n");
    }

    @PutMapping("/account/{id}")
    public void updateAccount(@PathVariable Integer id, @RequestParam("account") RequestEntity<AccountDTO> account) {
        accountService.change(id, dtoMapper.dtoAndAccount(account.getBody()));
        LOGGER.info("Put mapping account id has been initialized\n");
    }

    @DeleteMapping("/account/{id}")
    public void deleteAccount(@PathVariable Integer id) {
        accountService.delete(id);
        LOGGER.info("Delete mapping account id has been initialized\n");
    }
}
