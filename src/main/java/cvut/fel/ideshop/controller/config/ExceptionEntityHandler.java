package cvut.fel.ideshop.controller.config;

import cvut.fel.ideshop.exceptions.FieldInvalidException;
import cvut.fel.ideshop.exceptions.NotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

public class ExceptionEntityHandler extends ResponseEntityExceptionHandler {
    public static final String FIELD_MISSING = "FIELD_MISSING";
    public static final String FIELD_INVALID = "FIELD_INVALID";

    @ExceptionHandler(value = {NotFoundException.class})
    protected ResponseEntity<Object> handleNotFoundException(NotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getErrorCode());
    }

    @ExceptionHandler(value = {FieldInvalidException.class})
    protected ResponseEntity<Object> handleFieldInvalidException(FieldInvalidException ex) {
        return badRequest(FIELD_INVALID + ": " + ex.getMessage());
    }

    @ExceptionHandler(value = {RuntimeException.class})
    protected ResponseEntity<Object> handleFieldMissingException(RuntimeException ex) {
        return badRequest(FIELD_MISSING + ": " + ex.getMessage());
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        Error err = errorResponse();
        return badRequest(err);
    }

    private ResponseEntity<Object> badRequest(Error error) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    private ResponseEntity<Object> badRequest(String message) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
    }

    private Error errorResponse() {
        return new Error();
    }
}
