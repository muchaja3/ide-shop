package cvut.fel.ideshop.controller;

import cvut.fel.ideshop.dto.DTOMapper;
import cvut.fel.ideshop.dto.ProductDTO;
import cvut.fel.ideshop.model.database.entity.Product;
import cvut.fel.ideshop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.logging.Logger;

@RestController
public class ProductController {

    public static final Logger LOGGER = Logger.getGlobal();
    private final ProductService productService;
    private final DTOMapper dtoMapper;

    @Autowired
    public ProductController(ProductService productService, DTOMapper dtoMapper) {
        this.productService = productService;
        this.dtoMapper = dtoMapper;
    }

    @GetMapping("/product/{id}")
    public ResponseEntity<ProductDTO> getProductById(@PathVariable Integer id) {
        Product product = productService.findById(id);
        LOGGER.info("Get mapping product id has been initialized\n");
        return ResponseEntity.ok(dtoMapper.productAndDTO(product));
    }

    @GetMapping("/products")
    public ResponseEntity<ArrayList<ProductDTO>> getProducts() {
        Iterable<Product> products = productService.findAll();

        ArrayList<ProductDTO> productDTOs = new ArrayList<>();
        for (Product pr : products) {
            productDTOs.add(dtoMapper.productAndDTO(pr));
        }
        LOGGER.info("Get mapping products has been initialized\n");
        return ResponseEntity.ok(productDTOs);
    }

    @PostMapping("/newProduct")
    public void newProduct(@RequestParam("product") RequestEntity<ProductDTO> product) {
        productService.make(dtoMapper.dtoAndProduct(product.getBody()));
        LOGGER.info("Post mapping new product has been initialized\n");
    }

    @PutMapping("/product/{id}")
    public void updateProduct(@PathVariable Integer id, @RequestParam("product") RequestEntity<ProductDTO> product) {
        productService.change(id, dtoMapper.dtoAndProduct(product.getBody()));
        LOGGER.info("Put mapping product id has been initialized\n");
    }

    @DeleteMapping("/product/{id}")
    public void deleteProduct(@PathVariable Integer id) {
        productService.delete(id);
        LOGGER.info("Delete mapping product id has been initialized\n");
    }
}
