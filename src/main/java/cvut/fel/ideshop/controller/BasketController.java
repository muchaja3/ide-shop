package cvut.fel.ideshop.controller;

import cvut.fel.ideshop.dto.BasketDTO;
import cvut.fel.ideshop.dto.DTOMapper;
import cvut.fel.ideshop.model.database.entity.Basket;
import cvut.fel.ideshop.service.BasketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.logging.Logger;

@RestController
public class BasketController {

    public static final Logger LOGGER = Logger.getGlobal();
    private final BasketService basketService;
    private final DTOMapper dtoMapper;

    @Autowired
    public BasketController(BasketService basketService, DTOMapper dtoMapper) {
        this.basketService = basketService;
        this.dtoMapper = dtoMapper;
    }

    @GetMapping("/basket/{id}")
    public ResponseEntity<BasketDTO> getBasketById(@PathVariable Integer id) {
        Basket basket = basketService.findById(id);
        LOGGER.info("Get mapping basket id has been initialized\n");
        return ResponseEntity.ok(dtoMapper.basketAndDTO(basket));
    }

    @GetMapping("/baskets")
    public ResponseEntity<ArrayList<BasketDTO>> getBaskets() {
        Iterable<Basket> baskets = basketService.findAll();

        ArrayList<BasketDTO> basketDTOs = new ArrayList<>();
        for (Basket b : baskets) {
            basketDTOs.add(dtoMapper.basketAndDTO(b));
        }
        LOGGER.info("Get mapping baskets has been initialized\n");
        return ResponseEntity.ok(basketDTOs);
    }

    @PostMapping("/newBasket")
    public void newBasket(@RequestParam("basket") RequestEntity<BasketDTO> basket) {
        basketService.make(dtoMapper.dtoAndBasket(basket.getBody()));
        LOGGER.info("Post mapping new basket has been initialized\n");
    }

    @PutMapping("/basket/{id}")
    public void updateBasket(@PathVariable Integer id, @RequestParam("basket") RequestEntity<BasketDTO> basket) {
        basketService.change(id, dtoMapper.dtoAndBasket(basket.getBody()));
        LOGGER.info("Put mapping basket id has been initialized\n");
    }

    @DeleteMapping("/basket/{id}")
    public void deleteBasket(@PathVariable Integer id) {
        basketService.delete(id);
        LOGGER.info("Delete mapping basket id has been initialized\n");
    }
}
