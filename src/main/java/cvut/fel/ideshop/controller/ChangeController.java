package cvut.fel.ideshop.controller;

import cvut.fel.ideshop.dto.DTOMapper;
import cvut.fel.ideshop.dto.ChangeDTO;
import cvut.fel.ideshop.model.database.entity.Change;
import cvut.fel.ideshop.service.ChangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.logging.Logger;

@RestController
public class ChangeController {

    public static final Logger LOGGER = Logger.getGlobal();
    private final ChangeService changeService;

    private final DTOMapper dtoMapper;

    @Autowired
    public ChangeController(ChangeService changeService, DTOMapper dtoMapper) {
        this.changeService = changeService;
        this.dtoMapper = dtoMapper;
    }

    @GetMapping("/change/{id}")
    public ResponseEntity<ChangeDTO> getChangeById(@PathVariable Integer id) {
        Change change = changeService.findById(id);
        LOGGER.info("Get mapping change id has been initialized\n");
        return ResponseEntity.ok(dtoMapper.changeAndDTO(change));
    }

    @GetMapping("/changes")
    public ResponseEntity<ArrayList<ChangeDTO>> getChanges() {
        Iterable<Change> changes = changeService.findAll();

        ArrayList<ChangeDTO> changeDTOs = new ArrayList<>();
        for (Change ch : changes) {
            changeDTOs.add(dtoMapper.changeAndDTO(ch));
        }
        LOGGER.info("Get mapping changes has been initialized\n");
        return ResponseEntity.ok(changeDTOs);
    }

    @PostMapping("/newChange")
    public void newChange(@RequestParam("change") RequestEntity<ChangeDTO> change) {
        LOGGER.info("Post mapping new change has been initialized\n");
        changeService.make(dtoMapper.dtoAndChange(change.getBody()));
    }

    @PutMapping("/change/{id}")
    public void updateChange(@PathVariable Integer id, @RequestParam("change") RequestEntity<ChangeDTO> change) {
        changeService.change(id, dtoMapper.dtoAndChange(change.getBody()));
        LOGGER.info("Put mapping change id has been initialized\n");
    }

    @DeleteMapping("/change/{id}")
    public void deleteChange(@PathVariable Integer id) {
        LOGGER.info("Change mapping change id has been initialized\n");
        changeService.delete(id);
    }
}
