package cvut.fel.ideshop.cache.clusterCacheTtl;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.FileNotFoundException;

public class ClusterCacheListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent sce) {
        try {
            ClusterCache.init();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public void contextDestroyed(ServletContextEvent sce) {
        ClusterCache.destroy();
    }
}

