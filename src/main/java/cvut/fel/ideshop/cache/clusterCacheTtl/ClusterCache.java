package cvut.fel.ideshop.cache.clusterCacheTtl;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import java.io.FileNotFoundException;

public class ClusterCache {

    private static HazelcastInstance instance;

    public static void init() throws FileNotFoundException {
        Config cfg = new Config();

        instance = Hazelcast.newHazelcastInstance(cfg);
    }

    public static void destroy() {
        instance.shutdown();
    }

    public static HazelcastInstance getInstance() throws FileNotFoundException {

        if (instance == null) {
            init();
        }

        return instance;
    }
}

