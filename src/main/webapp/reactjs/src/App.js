import './App.css';

import NavigationBar from "./components/NavigationBar";
import Login from "./components/Login";
import Register from "./components/Register";
import Products from "./components/Products";
import Basket from "./components/Basket";
import AddProduct from "./components/AddProduct";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";

function App() {
  return (
    <Router>
        <NavigationBar/>
            <Routes>
                <Route path="/products" exact element={<Products/>} />
                <Route path="/addProduct" exact element={<AddProduct/>} /> -----------
                <Route path="/basket" exact element={<Basket/>} />
                <Route path="/login" exact element={<Login/>} />
                <Route path="/register" exact element={<Register/>} />
            </Routes>
    </Router>
  );
}

export default App;
