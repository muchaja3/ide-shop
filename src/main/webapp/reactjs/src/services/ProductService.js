import axios from 'axios'

const PRODUCTS_REST_API_URL = "http://localhost:8080/products/"; //nemame, potrebujeme doimplementovat (to same s users)

class ProductService {
    getProducts() {
        return axios.get(PRODUCTS_REST_API_URL);
    }
}
export default new ProductService();