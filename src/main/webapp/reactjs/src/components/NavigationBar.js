import React, {Component} from "react";
import {Link} from "react-router-dom";

export default class NavigationBar extends Component {
    render() {
        return (
            <header>
                <Link to={"/products"} className="navbar-brand">
                    <p className="shop-name">IDEshop</p>
                </Link>

                <Link to={"/addProduct"} className="nav-login">
                    <input type="button" className="blue" value="Add new product" />
                </Link>

                <div className="search">
                    <input type="text" className="searchbar" placeholder="Search..." />
                    <img src="../images/search.png" alt="" />
                </div>

                <Link to={"/basket"} className="nav-login">
                    <input type="button" className="blue" value="Basket" />
                </Link>

                <div className="auth">
                    <Link to={"/login"} className="nav-login">
                        <input type="button" className="white" value="Log in" />
                    </Link>
                    <Link to={"/register"} className="nav-login">
                        <input type="button" className="blue" value="Sign in" />
                    </Link>
                </div>
            </header>
        );
    }

}