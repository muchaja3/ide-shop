import React, {Component} from "react";
import axios from "axios";

export default class AddProduct extends Component {

        constructor(props) {
            super(props);
            this.state = this.initialState;
            this.productChange = this.productChange.bind(this);
            this.submitProduct = this.submitProduct.bind(this);
        }

        initialState = {
            name:'', price:'', description:''
        }

        submitProduct(event) {
            event.preventDefault();

            const product = {
                name: this.state.name,
                price: this.state.price,
                description: this.state.description,
            };
            axios.post("http://localhost:8080/?????????", product)
                .then(response => {
                    if (response.data != null) {
                        this.setState(this.initialState);
                        alert("Product added successfully");
                    }
                })
        }

        productChange(event) {
            this.setState({
                [event.target.name]:event.target.value
            });
        }

    render() {
        const {name, price, description} = this.state;

        return (
            <div className="add-product-form">
                <h2> Add product </h2>
                <form onSubmit={this.submitProduct} id="productFormId">
                    <div className="product-name">
                        <label>Product name</label>
                        <input type="text" name="name" placeholder="Enter product's name"
                               value={name}
                               onChange={this.productChange}
                        autoComplete="off"
                        required/>
                    </div>

                    <div className="product-price">
                        <label>Product price</label>
                        <input type="text" name="price" placeholder="Enter product's price"
                               value={price}
                               onChange={this.productChange}
                        autoComplete="off"
                        required/>
                    </div>

                    <div className="product-description">
                        <label>Product description</label>
                        <input type="text" name="description" placeholder="Enter product's description"
                               value={description}
                               onChange={this.productChange}
                        autoComplete="off"
                        required/>
                    </div>

                    <input className="blue" type="submit" value="Submit" />
                </form>
            </div>
        );
    }
}