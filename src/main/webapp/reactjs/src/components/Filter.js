import React, {Component} from "react";

export default class Filter extends Component {
    render() {
        return(
            <div className="filter">
                <span>Filter by</span>
                <form action="#">
                    <label htmlFor="price">Price:</label>
                    <select name="price" id="price">
                        <option value="X">X</option>
                        <option value="Y">Y</option>
                        <option value="Z">Z</option>
                    </select>

                    <label htmlFor="reviews">Reviews:</label>
                    <select name="reviews" id="reviews">
                        <option value="X">X</option>
                        <option value="Y">Y</option>
                        <option value="Z">Z</option>
                    </select>

                    <input type="submit" value="Filter" />
                </form>
            </div>
        );
    }
}