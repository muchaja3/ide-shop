import React, {Component} from "react";
import {Link} from "react-router-dom";

export default class Register extends Component {
    render() {
        return(
            <div className="auth-container">
                <h2>Sign in</h2>
                <form action="#">
                    <div className="username">
                        <label htmlFor="username">Username</label>
                        <input type="text" id="username" name="username" />
                    </div>

                    <div className="email">
                        <label htmlFor="email">E-mail</label>
                        <input type="text" id="email" name="email" />
                    </div>

                    <div className="password">
                        <label htmlFor="password">Password</label>
                        <input type="text" id="password" name="password" />
                    </div>

                    <input type="submit" className="blue" value="Sign in" />
                    <div className="or">Or</div>
                    <Link to={"/login"} className="nav-login">
                        <input type="button" className="white" value="Log in" />
                    </Link>
                </form>
            </div>
        );
    }
}