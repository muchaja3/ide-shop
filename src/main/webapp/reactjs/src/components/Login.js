import React, {Component} from "react";
import {Link} from "react-router-dom";

export default class Login extends Component {
    render() {
        return(
            <div className="auth-container">
                <h2>Log in</h2>
                <form action="#">
                    <label htmlFor="username">Username</label>
                    <input type="text" id="username" name="username" />

                    <label htmlFor="password">Password</label>
                    <input type="text" id="password" name="password" />

                    <input type="submit" className="blue" value="Log in" />
                    <div className="or">Or</div>
                    <Link to={"/register"} className="nav-login">
                        <input type="button" className="white" value="Sign in" />
                    </Link>
            </form>
            </div>
        );
    }
}
