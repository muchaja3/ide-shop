import React, {Component} from "react";
import ProductService from "../services/ProductService";

export default class Products extends Component {
    constructor(props) {
        super(props);

        this.state = {
            products:[
                { title: "VS Code", price: 20, description: "cool"},
                { title: "Intellij IDEA", price: 21, description: "amazing"},
                { title: "PyCharm", price: 20, description: "nice"}
            ]
        }
    }
/*
    componentDidMount() {
        ProductService.getProducts().then((response) =>
            this.setState({products: response.data})
        );
    }
*/
    render() {
        const {products} = this.state;
        var p;
        return(

        <section className="product-list">
            {p = products.map((product) => {
                return(
                    <div key={product.id} className="product-card">
                        <div className="main-info">
                            <div className="image">
                                {/*<img src="" alt="" />*/}
                            </div>
                            <div className="product-info">
                                <div className="title">{product.title}</div>
                                <div className="company">Company</div>
                                <div className="price-container">
                                    <div className="price">{product.price} $</div>
                                    <div className="view-btn">
                                        <input type="button" value="View details" />
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div className="product-description">
                                {product.description}
                            </div>
                        </div>
                    )
            })}
        </section>)
        // return (
        //     <section className="product-list">
        //         {p}
        //     </section>
        // );
    }
}
