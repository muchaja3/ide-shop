import React, {Component} from "react";

export default class Basket extends Component {
    render() {
        return(
            <div className="cart-container">
                <section className="cart">
                    <div className="item">
                        <p className="name">Intellij IDEA</p>
                        <div className="price-container basket">
                            <p className="price">$21.00</p>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                 className="bi bi-x-lg" viewBox="0 0 16 16">
                                <path
                                    d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z"/>
                            </svg>
                        </div>
                    </div>
                    <div className="item">
                        <p className="name">PyCharm</p>
                        <div className="price-container basket">
                            <p className="price">$20.00</p>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                 className="bi bi-x-lg" viewBox="0 0 16 16">
                                <path
                                    d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z"/>
                            </svg>
                        </div>
                    </div>
                    <div className="item">
                        <p className="name">Visual Studio Code</p>
                        <div className="price-container basket">
                            <p className="price">$20.00</p>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                 className="bi bi-x-lg" viewBox="0 0 16 16">
                                <path
                                    d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z"/>
                            </svg>
                        </div>
                    </div>
                    <div className="summary">
                        <p className="total">TOTAL</p>
                        <p className="totalPrice">$61.00</p>
                    </div>
                    <div className="checkout">
                        <input type="button" value="Checkout" />
                    </div>
                </section>
            </div>
        );
    }
}