CREATE TABLE "Role" (
    "id" char(5) NOT NULL,
    "role_type" char(1) NOT NULL UNIQUE,
    CONSTRAINT "Role_pk" PRIMARY KEY ("id")
);

CREATE TABLE "Account_Role" (
    "account_id" int NOT NULL UNIQUE,
    "role_id" char(5) NOT NULL UNIQUE
);

CREATE TABLE "Account" (
    "id" int NOT NULL,
    "email" varchar(100) NOT NULL CHECK (email LIKE '_%@_%.__%'),
    "firstname" varchar(10) NOT NULL,
    "lastname" varchar(20) NOT NULL,
    "password" varchar(100) NOT NULL,
    CONSTRAINT "Account_pk" PRIMARY KEY ("id")
);

CREATE TABLE "Product" (
    "id" int NOT NULL,
    "name" varchar(50) NOT NULL,
    "price" varchar(255) NOT NULL,
    "description" varchar(100) NOT NULL,
    CONSTRAINT "Product_pk" PRIMARY KEY ("id")
);

CREATE TABLE "Change" (
    "id" int NOT NULL,
    "old_name" varchar(50) NOT NULL,
    "old_price" varchar(50) NOT NULL,
    "old_description" varchar(100) NOT NULL,
    CONSTRAINT "Change_pk" PRIMARY KEY ("id")
);

CREATE TABLE "Payment" (
    "id" int NOT NULL,
    "info" varchar(255) NOT NULL,
    "type" varchar(100) NOT NULL,
    CONSTRAINT "Payment_pk" PRIMARY KEY ("id")
);

CREATE TABLE "Account_Basket" (
    "account_id" int NOT NULL UNIQUE,
    "basket_id" int NOT NULL UNIQUE
);

CREATE TABLE "Basket" (
    "id" int NOT NULL,
    "products_amount" int NOT NULL,
    "total_price" varchar(255) NOT NULL,
    CONSTRAINT "Basket_pk" PRIMARY KEY ("id")
);

CREATE TABLE "Basket_Payment" (
    "basket_id" int NOT NULL UNIQUE,
    "payment_id" int NOT NULL UNIQUE
);

CREATE TABLE "Basket_Product" (
    "product_id" int NOT NULL UNIQUE,
    "basket_id" int NOT NULL UNIQUE
);

CREATE TABLE "Product_Change" (
    "change_id" integer NOT NULL,
    "product_id" integer NOT NULL
);

ALTER TABLE "Account_Role" ADD CONSTRAINT "Account_Role_fk0" FOREIGN KEY ("account_id") REFERENCES "Account"("id")
    ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE "Account_Role" ADD CONSTRAINT "Account_Role_fk1" FOREIGN KEY ("role_id") REFERENCES "Role"("id")
    ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE "Account_Basket" ADD CONSTRAINT "Account_Basket_fk0" FOREIGN KEY ("account_id") REFERENCES "Account"("id")
    ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE "Account_Basket" ADD CONSTRAINT "Account_Basket_fk1" FOREIGN KEY ("basket_id") REFERENCES "Basket"("id")
    ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE "Basket_Payment" ADD CONSTRAINT "Basket_Payment_fk0" FOREIGN KEY ("basket_id") REFERENCES "Basket"("id")
    ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE "Basket_Payment" ADD CONSTRAINT "Basket_Payment_fk1" FOREIGN KEY ("payment_id") REFERENCES "Payment"("id")
    ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE "Basket_Product" ADD CONSTRAINT "Basket_Product_fk0" FOREIGN KEY ("product_id") REFERENCES "Product"("id")
    ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE "Basket_Product" ADD CONSTRAINT "Basket_Product_fk1" FOREIGN KEY ("basket_id") REFERENCES "Basket"("id")
    ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE "Product_Change" ADD CONSTRAINT "Product_Change_fk0" FOREIGN KEY ("change_id") REFERENCES "Change"("id")
    ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE "Product_Change" ADD CONSTRAINT "Product_Change_fk1" FOREIGN KEY ("product_id") REFERENCES "Product"("id")
    ON UPDATE CASCADE ON DELETE SET NULL;
