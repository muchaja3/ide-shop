# This program was created as a semester project of the NSS
 
## (CTU - SIT summer semester 2022)

### Project name: IDE E-Shop (with UI)
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
### Authors: 
### Dmitry Rastvorov, Sabina Balaeva,
### Jan Mucha, Volodymyr Khodatskyi
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
### The diagram and documentation of our project can be found in the [doc folder](https://gitlab.fel.cvut.cz/muchaja3/ide-shop/-/blob/master/doc/Online_obchod_na_prodej_IDE.pdf).
#### ATTENTION: Description of this project in Czech language. 
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
### This README serves for a brief description of the project , the technologies used, the launch of the application.
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
#### • Used technologies:
     • Java
     • Spring Boot
     • REST
     • JavaScript (ECMAScript)
     • React
     • PostgreSQL
     • HTML, CSS
~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
#### • The project uses the Waterfall model as well as SpringBoot to connect UI, Controller, Service, DAO.
#### • javadoc, PostgreSQL and database model diagram can be found in [doc folder](https://gitlab.fel.cvut.cz/muchaja3/ide-shop/-/tree/master/doc).
#### • PostgreSQL: connection with DBS database.
~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
#### • Used database: Relational.
~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
#### • Messaging principle: Was not used.
~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
#### • Security: Was not used.
~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
#### • Interceptors: Logging.
~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
#### • Actuator: Actuator has been used in [Main class](https://gitlab.fel.cvut.cz/muchaja3/ide-shop/-/blob/master/src/main/java/cvut/fel/Main.java).
~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
#### • Heroku: loading...
~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
#### • Architecture (event based): loading...
~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
#### • Initiation procedure: 
        1) To run this project You need to have an Internet connection
        and at least one of these browsers: Safari, Chrome, FireFox.
        ------------------------------------------------------------
        2) To start the project You need to open the file
        "src/main/java/cvut/fel/" and open Main.java
        ------------------------------------------------------------
        3) After initialization You will open the website,
        where You will be invited as a user and can view
        the products on the website and read their descriptions. 
        Also when you press "Exit" the program will stop.
~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
#### • Patterns:
#### We used several design patterns in our project:
        1) Facade pattern
        2) Builder pattern
        3) Dependency Injection pattern
        4) Observer pattern
        5) State pattern
~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
#### • For each team member 2 UC (4 × 2 = 8):
1. Create a product
2. Change the product details
3. Delete product
4. Show product change history
5. Change account login details
6. Delete account
7. Temporarily block the account
8. View product list
9. Search for a product 
10. Filter products
11. Register
12. Log in
13. Change your login details
14. Buy the offered product
15. View products purchased
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
### Implementation by Dmitry for C grade:

#### • JUnit tests: JUnit tests for service can be found in the [test folder](https://gitlab.fel.cvut.cz/muchaja3/ide-shop/-/tree/master/src/test/java/cvut/fel).
#### • Cache: The cache can be found in the [cache folder](https://gitlab.fel.cvut.cz/muchaja3/ide-shop/-/tree/master/src/main/java/cvut/fel/cache) and in the [Executor class](https://gitlab.fel.cvut.cz/muchaja3/ide-shop/-/blob/master/src/main/java/cvut/fel/Executor.java).
#### • SpringFox: SpringFox was used in the  [SpringFoxConfig class](https://gitlab.fel.cvut.cz/muchaja3/ide-shop/-/blob/master/src/main/java/cvut/fel/config/SpringFoxConfig.java).
#### • Log structure with code, system (BE/FE), Message, ID (using builder): Was not implmemented.
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
### Implementation by Volodymyr for E grade:

#### • Use of ElasticSearch: loading...
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
#### Enjoy 🤗
